import React from 'react';

class Clock extends React.Component {
  constructor(){
    super();
    this.state = {
      date: new Date()
    };
  }

  render() {
    const {date} = this.state;
    return (
      <div>
        <h1> Clock </h1>
        <div className = "date">
          <h3> Time: </h3>
          <br/>
          <br/>
          <h3>{date.getHours()}:{date.getMinutes()}:{date.getSeconds()}</h3>
        </div>
        <div className = "date">
          <h3> Date: </h3>
          <h3> {date.getMonth() + 1} / {date.getDate()} / {date.getFullYear()}</h3>
        </div>
      </div>

    );

  }

  componentDidMount(){
    console.log('mount');
    this.intervalId = setInterval(this.tick.bind(this), 1000);
  }

  componentWillUnmount(){
    console.log('unmount');
    clearInterval(this.intervalId);
    this.intervalId = 0;
  }

  tick(){
    this.setState({date: new Date()});
  }


}



export default Clock;

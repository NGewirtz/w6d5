import React from 'react';

const TabBody = ({body}) => {
  return (
    <article>{body}</article>
  )
}

export default TabBody;

import React from 'react';
import Header from './header';
import TabBody from './tab_body';

class Tab extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      body: this.props.panes[0].body
    };
  }

  render() {
    const headers = this.props.panes.map( (header, idx) => {
      const selected = this.state.index === idx;
      return (
          <Header title={header.title} body={header.body} key={idx}
            selectMe ={() =>{
              this.setState({ index: idx, body: header.body});
            }}
            selected={selected}/>
        );
    });

    return (
      <div>
        <ul className = "tabs-body">
            {headers}
        </ul>
        <TabBody body={this.state.body}/>
      </div>
    );
  }
}


export default Tab;

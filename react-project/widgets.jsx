import React from 'react';
import ReactDOM from 'react-dom';
import Clock from './frontend/clock';
import Tab from './frontend/tabs';

const Root = () => {
  const array = [{title: "first", body: "this is body"},{title: "second", body: "this is more content"}];
  return (
    <div>
      <Clock />
      <Tab panes={array}></Tab>
    </div>
  );
};



document.addEventListener('DOMContentLoaded', () => {

  const main = document.getElementById("main");
  ReactDOM.render(<Root />, main );
});
